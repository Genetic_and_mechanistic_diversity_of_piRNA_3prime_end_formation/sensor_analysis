## R script to plot trigger sensor coverage

#get in variables from bash
args  =  commandArgs(TRUE);
argmat  =  sapply(strsplit(args, "="), identity)

for (i in seq.int(length=ncol(argmat))) {
  assign(argmat[1, i], argmat[2, i])
}

#available variables
print(ls())

pdf.options(useDingbats = FALSE)

#prepare name of bedgraph files and trigger sequence file
bedp=c(NAME,"_plus.bedgraph")
bedm=c(NAME,"_minus.bedgraph")
bed3end=c(NAME,"_plus_3end.bedgraph")
plusname=paste(bedp, collapse="")
minusname=paste(bedm, collapse="")
threeendname=paste(bed3end, collapse="")

z=c(NAME,".pdf")
pdfname=paste(z, collapse="")

#get sequence and bedgraph files and combine plus and minus strand counts
TR=read.delim(file=plusname,header=FALSE)
TRmin=read.delim(file=minusname,header=FALSE)
TR3end=read.delim(file=threeendname,header=FALSE)
names(TR)=c("name","pos","plus_counts")
TR$minus_counts=TRmin$V3
TR$ends=TR3end$V3

#Set limits for reads counts to display
ymin=-50
ymax=50

#plot 5'ends, xlim defines the positions (bp) in the sensor to display
pdf(file=pdfname)
plot(TR$pos,TR$plus_counts,pch=20,cex=0.5,ylim=c(ymin,ymax),xlim=c(725,875), type="h", las=1,
xlab="position in sensor [bp]",ylab="5'end coverage normalized to 1mio miRNAs",main=names(seq))
lines(TR$pos,TR$minus_counts,pch=20,cex=0.5,col="blue",type="h")

#add 3'ends
lines(TR$pos,TR$ends*(1),pch=20,cex=0.5, col="red",type="h")
legend(x="topright",  lty=c(1,1,1,1,NA,NA,NA), lwd=2, col=c("blue","black","red",NA), legend=c("antisense piRNA 5' ends","sense piRNA 5' ends","sense piRNA 3' ends"))
dev.off()
