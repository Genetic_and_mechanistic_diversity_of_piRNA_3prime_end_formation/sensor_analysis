module load bedtools/2.25.0
module load samtools/0.1.18
module load fastx-toolkit/0.0.13
module load bowtie/0.12.9
module load R/2.15.3

set -u

#-------------------------------------------------------------------------------------------------------
#presetting of variables
NAMEforANALYSIS=
rawFOLDER=
rawTMP=
pFASTA=
nFASTA=
miRNAnorm=
MMs=0
NSLOTS=10
#path to input file
pINPUT=

#-------------------------------------------------------------------------------------------------------
#prepare all vairables
FOLDER="${rawFOLDER}${NAMEforANALYSIS}/${NAMEforANALYSIS}"
TMP="${rawTMP}/${NAMEforANALYSIS}/${NAMEforANALYSIS}"
INPUT=
FASTA=

#-------------------------------------------------------------------------------------------------------
#create all folders
mkdir -p "${rawFOLDER}${NAMEforANALYSIS}"
mkdir -p $TMP

#-------------------------------------------------------------------------------------------------------
#use fasta file provided to generate index for sensors
bowtie-build -f $FASTA "${FOLDER}_in"

#use fasta file to determine the length of the sensor contruct to gnerate a genome file for genomeCoverageBed
LENGTH=`cat $FASTA | sed '1d' | wc -c` 
#Report length of construct
echo -e $nFASTA$'\t'$LENGTH
#Write genome file for mapping
echo -e $nFASTA$'\t'$LENGTH > "${FOLDER}_trigger_chrom_sizes"

#-------------------------------------------------------------------------------------------------------
#De-collapse fasta input file & generate a trimmed fa. file for mapping of the triggers piRNAs
gunzip -c $INPUT| awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0" ":$0 }'  | awk 'length($2)>=20 {split($1,count,"~"); for(i=1;i<=count[2];i++) print $1"\n"$2 }' > ${TMP}.fa
cat ${TMP}.fa | fastx_trimmer -f 1 -l 21 -m 20 > ${TMP}_trimmed.fa

gunzip -c $INPUT | awk '/^>/&&NR>1{print "";}{ printf "%s",/^>/ ? $0" ":$0 }'  | awk '{print} ' | awk 'length($2)>=20 {split($1,count,"~"); for(i=1;i<=count[2];i++) print $1"\n"$2 }' > ${TMP}.fa

#map to trigger sensors
bowtie -f -a -v ${MMs} -S --best --strata -p $NSLOTS "${FOLDER}_in" ${TMP}_trimmed.fa | samtools view -bS - | bamToBed -i - | sort -k1,1 -k2,2n | awk '{OFS="\t"; print $1,$2,$3,$4,$3-$2,$6}' > ${FOLDER}_mapped_${MMs}_trimmed.bed
bowtie -f -a -v ${MMs} -S --best --strata -p $NSLOTS "${FOLDER}_in" ${TMP}.fa | samtools view -bS - | bamToBed -i - | sort -k1,1 -k2,2n | awk '{OFS="\t"; print $1,$2,$3,$4,$3-$2,$6}' > ${FOLDER}_mapped_${MMs}.bed

#make basepair bed file using only 5'ends of piRNAs
awk '$6=="+" {print $1, $2, $2+1, $6}' ${FOLDER}_mapped_${MMs}.bed | sed 's/ /\t/g' | genomeCoverageBed -d -i stdin -g "${FOLDER}_trigger_chrom_sizes" | awk -vSIZE=${miRNAnorm} '{print $1"\t"$2"\t"$3/SIZE}'> ${FOLDER}_plus.bedgraph
awk '$6=="-" {print $1, $3-1, $3, $6}' ${FOLDER}_mapped_${MMs}_trimmed.bed | sed 's/ /\t/g' | genomeCoverageBed -d -i stdin -g "${FOLDER}_trigger_chrom_sizes" | awk -vSIZE=${miRNAnorm} '{print $1"\t"$2"\t"(-$3/SIZE)}' > ${FOLDER}_minus.bedgraph

#get bedgraph file for 3'ends on plus strand
awk '$6=="+" {print $1, $3-1, $3, $6}' ${FOLDER}_mapped_${MMs}.bed | sed 's/ /\t/g' | genomeCoverageBed -d -i stdin -g ${FOLDER}_trigger_chrom_sizes | awk -vSIZE=${miRNAnorm} '{print $1"\t"$2"\t"$3/SIZE}'> ${FOLDER}_plus_3end.bedgraph

#Run the R script - changing of x and y limits can be done in the R script
Rscript /groups/brennecke/JS_RH_3end/git/sensor_analysis/sensor_plot.R NAME=${FOLDER} FASTA=${FASTA}
