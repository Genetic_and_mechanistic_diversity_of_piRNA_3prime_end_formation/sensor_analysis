# Scripts for mapping to the sensor constructs

**sensor_analysis.sh**

This scripts uses a .fasta file to map to sensor constructs. The sensor mapping index is generated from a .fasta file containing the sequence of the sensor construct.


## Used tools:

fastx-toolkit/0.0.13

bedtools/2.25.0

R/2.15.3

samtools/0.1.18

bowtie/0.12.9

## Input:

As input file a fasta.gz file was used. This file contains all sequenced reads collapsed. For generation of the sensor mapping a .fasta file containing the name and the sequence of the exogenous construct have to be provided.

The .fasta.gz file looks as follows:

> \>HWI-ST1253F-0259:8:1113:14526:16858#26166_TACGAGCCGGTGACATTGCTCGGGTG?TE_AS@u~8

> TACGAGCCGGTGACATTGCTCGGGTG

Line1: Identifier – composed of –Sequencing identifier _ sequence of read ? annotated category @ unique or multimapper ~ read count

Line2: Sequence

## Variables which need to be set in sensor_analysis.sh: 

**NAMEforANALYSIS**= The name for input files and output generated

**rawFOLDER**= Folder which contains raw files which can be used for further analyses 

**rawTMP**=Temporary files which can be deleted afterwards

**miRNAnorm**= miRNA PPM cutoff according to library mapping as describe in Materials and Methods. Eg.: 5.36

**NSLOTS**= CPUs available for the analysis

**pINPUT**=path for input files used in this analysis

**INPUT**= specify the exact input file eg: INPUT="${pINPUT}${NAMEforANALYSIS}.fa.gz"

**pFASTA**=path to the .fa file of the sensor sequene

**nFASTA**=Name of the sensor sequence

**MMs**=miss matches tolerated for mapping

## Variables which need to be set in sensor_plot.R: 

**ymin**= lower axis limit for the display of read counts eg.: -50
**ymax**= upper axis limit for the display of read counts eg.: 50

**xlim**= region to display of the sensor construct eg.: c(725,875)

## Output

The tool will generate .pdf files containing barplots displaying the normalized sequencing counts mapping to the given sequence in the sensor .fasta file. 